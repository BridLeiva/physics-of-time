This project is a Python implementation of the physics of time puzzle, just for fun!

https://www.quantamagazine.org/20160913-the-physics-of-time-puzzle/


-----------------------------------------------------------------------------------


In order to run the simulation you must have python 3 installed. 
On windows I recommend the [Anaconda python distribution](https://www.continuum.io/downloads). 
On linux you should use the package manager of your own distro.

You also need pygame, which you can install after installing python with:

    pip install pygame
    
After you have the necessary software, you can run the simulation with:

    python PhOTGame.py
    
Or just double click on start.sh if you are in linux or windows 10.

Please send your questions, suggestions or contributions at bridleiva at gmail.com .