#!/usr/bin/env python

import pygame, sys, random

class Node:
    """This class represents a node of the grid."""
    def __init__(self, x, y):
        self.x, self.y = (x, y) #These are the coordinates of the node on the grid.

        #And these are it's coordinates on pygame's screen.
        self.scrX = x * 120 + 20
        self.scrY = 480 - (y * 110 + 20)

        # Additionally, each node must know which other nodes are it's neighbors.
        # which will be set by the grid class after creating the nodes.



class Bar:
    """This class represent each one of the atoms on the simulation. 
    Which are bars on the lcd universe."""
    def __init__(self, node1, node2, screen):
        #Our bar consists on two nodes
        self.nodes = [node1,node2]
        self.screen = screen # ...and a screen so it can handle it's own drawing.

    def move(self):
        """Moving a bar to a new location on the grid"""

        # Since both nodes are interchangeable, lets just move the node in position
        # 0 to one of it's neighbors and then move the node in 1 to one of the neighbors
        # of the new node in 0.
        self.nodes[0] = random.choice( self.nodes[0].neighbors )
        self.nodes[1] = random.choice( self.nodes[0].neighbors )

    def draw(self):
        """Draws the bar on the screen"""
        points = [ (n.scrX , n.scrY) for n in self.nodes ]
        pygame.draw.lines(self.screen, (255,0,0), False, points , 2 )



class Grid:
    """Represents the grid for the simulation."""
    def __init__(self, screen):
        self.screen = screen #Screen to handle drawing

        #Generating the nodes and their setting their neighbors.
        self.nodes = [ Node(x,y) for x in range(0,6) for y in range(0,5) ]
        for node in self.nodes: node.neighbors = self.getNeighbors(node)

        #Coodenates for generationg the bars on the center of the screen.
        barCoords = [ (2,1, 3,1), (2,1, 2,2), (3,1, 3,2) # beginning from the bottom left
            , (2,2, 3,2), (2,2, 2,3), (3,2, 3,3)
            , (2,3, 3,3) ]

        # Creates the bars according to the barcoords
        self.bars = [ Bar(self.getNode(b[0],b[1]), self.getNode(b[2],b[3]), self.screen) 
                            for b in barCoords ]
        self.draw()

    def getNode(self, x, y):
        """Returns a node by coordinates if exists."""
        for node in self.nodes:
            if (node.x, node.y) == (x, y): return node

    def getNeighbors(self, node):
        """Returns the neighbors of a given node."""
        return [ self.getNode(node.x + dx, node.y + dy) 
            for dx in (-1, 0, 1) 
            for dy in (-1, 0, 1) 
            if (dx + dy)**2 == 1 #So one and only one of the values is either 1 or -1 
                and not self.getNode(node.x + dx, node.y + dy) is None ]
            
    def draw(self):
        """Draws the grid and then calls for the bars to draw themselves."""
        screen.fill((255,255,255)) # Clears the screen.
        
        #Drawing the grid.
        for x in range(0,6): # Vertical lines
            xLen = x * 120 + 20
            points = [ (xLen, 20), (xLen,460) ]
            pygame.draw.lines(self.screen, (200,200,200), False, points , 2 )

        for y in range(0,5): #Horizontal lines
            yLen = y * 110 + 20
            points = [ (20, yLen), (620,yLen) ]
            pygame.draw.lines(self.screen, (200,200,200), False, points , 2 )

        for bar in self.bars: bar.draw() #Drawing the bars.

        pygame.display.update()

if __name__=='__main__':
    screen = pygame.display.set_mode((640,480))
    grd = Grid(screen)

    # Entering the event loop
    while True:
        #Checking for the quit event
        for evt in pygame.event.get():
            if evt.type == pygame.QUIT:
                pygame.quit(); sys.exit()

        # Waith for one second ...
        pygame.time.delay(1000)

        #... then evolve the universe by moving the bars...
        for bar in grd.bars: bar.move()

        grd.draw() # ... and redraw the grid.
